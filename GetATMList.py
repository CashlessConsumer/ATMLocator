import requests
import json
import csv
import sqlite3
import glob
import os
import logging

data_folder = './data/'
PROCESSED_COUNT = 19095

logging.basicConfig(filename='GetATMList.log',level=logging.INFO, datefmt="%Y-%m-%d %H:%M:%S")

def fetch_atm_data():
    logging.info("Method fetch_atm_data")
    input_filename = 'PinCode_statecode.csv'
    with open(input_filename,'r') as f:
        reader = csv.reader(f)
        pin_code_list = list(reader)
        skip = 1
        i = 0
        for pin in pin_code_list:
            if(skip is 1 and i < PROCESSED_COUNT):
                i = i + 1
                continue
            else:
                skip = 0
            if len(glob.glob(data_folder + str(pin[0]) + "*.json")) > 0:
                logging.debug("Skipping " + str(pin[0]))
                continue
            data = {'pincode': pin[0], 'state_id': pin[1]}
            try:
                get_atm_in_pin_code(data)
            except:
                continue


def get_atm_in_pin_code(data):
    logging.debug("Getting data for " + str(data['pincode']))
    #url = 'https://www.npci.org.in/atm/locator/demo?state=' + data['state_id'] '&pincode=' + data['pincode']
    #r = requests.get(url)
    r = requests.post('https://npci.org.in/npci_atm_locator/Services/get_searched_data',data)
    #logging.debug(r)
    if r.json()['status']:
        filename = data_folder + str(data['pincode']) + '_' + str(r.json()['total_count']) + '.json'
        with open(filename,'w') as fp:
            json.dump(r.json()['data'],fp)


def dump_data_to_db():
    logging.info('Method dump_data_to_db')
    db = sqlite3.connect('ATM_DB.sqlite')
    file_dir = os.path.dirname(os.path.realpath('__file__'))

    for datafile in os.listdir('data'):
        filename = os.path.join(file_dir,'data', datafile)
        atms = json.load(open(filename))

        for atm in atms:
            columns = list(atm.keys())
            query = "insert into atm_info ({0}) values ({1})"
            query = query.format(",".join(columns), "?," * len(columns))
            query = query[:-2] + ')'
            keys = tuple(atm[c] for c in columns)
            c = db.cursor()
            c.execute(query,keys)
            c.close()
        db.commit()

def getCount():
	files = os.listdir('/workspace/ATMLocator/data')
	count = 0
	for file in files:
		count = count + int(file.split('.')[0].split('_')[1])
	print('Number of distinct PIN Codes served ' + str(len(files)))
	print('Number of ATMs ' + str(count))

if __name__ == "__main__":
    logging.info('Starting GetATMList.py')
    fetch_atm_data()
    dump_data_to_db()
    #get_atm_in_pin_code(data = {'pincode': 562117, 'state_id': 17})